<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://localhost/wordpress/wp-json/wp/v2/posts",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\n\t  \"title\": \"prova9\",\n\t  \"content\": \" content\"\n\t\n\t\n}\n\n\n\n\n\n\n\n\n\n",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic d29yZHByZXNzOndvcmRwcmVzcw==",
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: de08c3a1-607c-291c-86e4-56d2831e661b"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

