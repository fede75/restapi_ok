<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://testcab.openview.it/wp-json/wp/v2/media",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic ZmVkZXJpY29yaXp6YXRvNzVAZ21haWwuY29tOlZhbGxpc25lcmlfMTY2MQ==",
    "cache-control: no-cache",
    "content-disposition: attachment;filename=\"ppp.PNG\"",
    "content-type: image/png",
    "postman-token: 8d4bf926-ca59-b7f2-00fa-b5911bd61e3b"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}