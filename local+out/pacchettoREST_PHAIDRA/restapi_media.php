
<?php


$nomefile = $_POST['nomefile'];
$codice = $_POST['codice'];

$url = 'https://fc.cab.unipd.it/fedora/objects/o:'.$codice.'/methods/bdef:Asset/getThumbnail';
echo $url;
$curl_handle=curl_init($url);
if ($curl_handle === null) {
	echo "Manca curl";
	die;
}
curl_setopt($curl_handle, CURLOPT_HEADER, 0);
curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_handle, CURLOPT_BINARYTRANSFER,1);


curl_setopt($curl_handle, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);

$buffer = curl_exec($curl_handle);
curl_close($curl_handle);




$curl = curl_init();
$data = $buffer;

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://testcab.openview.it/wp-json/wp/v2/media",/*testcab.openview.it/wp-json/wp/v2/media*/
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic ZmVkZXJpY29yaXp6YXRvNzVAZ21haWwuY29tOlZhbGxpc25lcmlfMTY2MQ==",/*Basic ZmVkZXJpY29yaXp6YXRvNzVAZ21haWwuY29tOlZhbGxpc25lcmlfMTY2MQ==*/
    "cache-control: no-cache",
    "content-disposition: attachment; filename=$nomefile.jpg",
    "content-type: image/jpg",
	
	
	
  ),
  CURLOPT_POSTFIELDS => $data,
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}

